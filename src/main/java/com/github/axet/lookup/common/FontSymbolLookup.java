package com.github.axet.lookup.common;

import java.awt.Rectangle;

import org.apache.commons.lang.math.IntRange;

public class FontSymbolLookup {
    public int x;
    public int y;
    public FontSymbol fs;
    public double g;
    
    public FontSymbolLookup(FontSymbol fs, int x, int y, double g) {
        this.fs = fs;
        this.x = x;
        this.y = y;
        this.g = g;
    }
    
    public int size() {
        return fs.getHeight() * fs.getWidth();
    }
    
    public boolean cross(FontSymbolLookup f) {
        int minX1 = x;
        int minY1 = y;
        int maxX1 = x + fs.getWidth();
        int maxY1 = y + fs.getHeight();
    
        int minX2 = f.x;
        int minY2 = f.y;
        int maxX2 = f.x + f.fs.getWidth();
        int maxY2 = f.y + f.fs.getHeight();
        
        // If one rectangle is on left side of other
        if (maxX1 < minX2 || maxX2 < minX1)
            return false;
    
        // If one rectangle is above other
        if (minY1 > maxY2 || minY2 > maxY1)
            return false;
    
        return true;
    }
    
    public double intersectionAreaRatio(FontSymbolLookup f) {
        int minX1 = x;
        int minY1 = y;
        int maxX1 = x + fs.getWidth();
        int maxY1 = y + fs.getHeight();
    
        int minX2 = f.x;
        int minY2 = f.y;
        int maxX2 = f.x + f.fs.getWidth();
        int maxY2 = f.y + f.fs.getHeight();
        
        return Math.max(0, Math.min(maxX1, maxX2) - Math.max(minX1, minX2))* Math.max(0, Math.min(maxY1, maxY2) - Math.max(minY1, minY2));
    }
    
    public boolean yCross(FontSymbolLookup f) {
        int minY1 = y;
        int maxY1 = y + fs.getHeight();
        int minY2 = f.y;
        int maxY2 = f.y + f.fs.getHeight();
        
        return !(minY1 > maxY2 || minY2 > maxY1);
    }
    
    public int getWidth() {
        return fs.getWidth();
    }
    
    public int getHeight() {
        return fs.getHeight();
    }
    
    @Override
    public String toString() {
        return fs.fontSymbol;
    }
}
