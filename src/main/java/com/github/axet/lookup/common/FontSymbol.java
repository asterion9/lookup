package com.github.axet.lookup.common;

import java.awt.image.BufferedImage;

public class FontSymbol {
    public FontFamily fontFamily;
    public String fontSymbol;
    public ImageBinaryScale image;

    public FontSymbol(FontFamily ff, String fs, BufferedImage i) {
        this.fontFamily = ff;
        this.fontSymbol = fs;
        this.image = new ImageBinaryGreyScale(i);
    }

    public int getHeight() {
        return image.getHeight();
    }

    public int getWidth() {
        return image.getWidth();
    }

    public String toString() {
        return fontSymbol;
    }
}
