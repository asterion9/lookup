package com.github.axet.lookup;

import com.github.axet.lookup.common.ClassResources;
import com.github.axet.lookup.common.FontFamily;
import com.github.axet.lookup.common.FontSymbol;
import com.github.axet.lookup.common.FontSymbolLookup;
import com.github.axet.lookup.common.ImageBinary;
import com.github.axet.lookup.common.ImageBinaryGrey;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OCR extends OCRCore {

    public OCR(float threshold) {
        super(threshold);
    }

    /**
     * set sensitivity
     * 
     * @param threshold
     *            1 - exact match. 0 - not match. -1 - opposite difference
     */
    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    public float getThreshold() {
        return threshold;
    }

    /**
     * Load fonts / symbols from a class directory or jar file
     * 
     * @param c
     *            class name, corresponded to the resources. com.example.MyApp.class
     * @param path
     *            path to the fonts folder. directory should only contain folders with fonts which to load
     * 
     */
    public void loadFontsDirectory(Class<?> c, File path) {
        ClassResources e = new ClassResources(c, path);

        List<String> str = e.names();

        for (String s : str)
            loadFont(c, new File(path, s));
    }

    /**
     * Load specified font family to load
     * 
     * @param c
     *            class name, corresponded to the resources. com.example.MyApp.class
     * @param path
     *            path to the fonts folder. directory should only contain folders with fonts which to load.
     * 
     */
    public void loadFont(Class<?> c, File path) {
        ClassResources e = new ClassResources(c, path);

        List<String> str = e.names();

        for (String s : str) {
            File f = new File(path, s);

            InputStream is = c.getResourceAsStream(ClassResources.toResourcePath(f));

            String symbol = FilenameUtils.getBaseName(s);

            try {
                symbol = URLDecoder.decode(symbol, "UTF-8");
            } catch (UnsupportedEncodingException ee) {
                throw new RuntimeException(ee);
            }

            String name = path.getName();
            loadFontSymbol(name, symbol, is);
        }
    }
    
    /**
     * load font files (ttf).
     * convert ttf files to a collection of image file of each character in 16px greyscale as expected
     *
     * @param clazz    the class (must match package directory of the fonts folder)
     * @param path     sub path of the font folder
     * @param fontSize desired font size for the file (to avoid scaling later)
     */
    public void loadFontTtf(Class<?> clazz, File path, int fontSize) {
        Set<Font> fonts = loadFontTtfFolder(clazz, path);
        FontRenderContext frc = new FontRenderContext(new AffineTransform(), RenderingHints.VALUE_TEXT_ANTIALIAS_ON, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        String textSample = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-%,";
        Path outPath = Paths.get("target", "roboto");
        outPath.toFile().mkdirs();
        fonts.stream()
                .map(font -> font.deriveFont((float)fontSize))
                .forEach(font ->
                        Stream.of(textSample.split(""))
                                .forEach(c -> {
                                    TextLayout tl = new TextLayout(c, font, frc);
                                    Rectangle2D bounds = tl.getBounds();
                                    BufferedImage bufferedImage = new BufferedImage((int) Math.ceil(bounds.getWidth())+1, (int) Math.ceil(bounds.getHeight())+1, BufferedImage.TYPE_BYTE_GRAY);
                                    Graphics2D graphics = bufferedImage.createGraphics();
                                    graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VRGB);
                                    graphics.setBackground(Color.BLACK);
                                    graphics.setColor(Color.WHITE);
                                    tl.draw(graphics, (float)Math.floor(-bounds.getMinX())+1, (float)Math.floor(-bounds.getY())+1);
                                    loadFontSymbol(font.getFontName(), c, bufferedImage);
                                    try {
                                        writeSymbol(font.getFontName(), c, bufferedImage, outPath);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                })
                );
        
        
    }
    
    private File writeSymbol(String fontName, String c, BufferedImage bufferedImage, Path outputFolder) throws IOException {
        File outputfile = outputFolder.resolve("lookup_" + fontName + "_" + URLEncoder.encode(c, "UTF-8") + "_" + (int)c.charAt(0) + ".png").toFile();
        //File tempFile = Files.createTempFile("lookup_" + fontName + "_", ".png").toFile();
        ImageIO.write(bufferedImage, "png", outputfile);
        System.out.println("tempFile " + c + " = " + outputfile);
        return outputfile;
    }
    
    private Set<Font> loadFontTtfFolder(Class<?> c, File path) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ClassResources e = new ClassResources(c, path);
       return e.names().stream()
                .map(file -> new File(path, file))
                .map(ClassResources::toResourcePath)
                .map(c::getResourceAsStream)
                .map(is -> {
                    try (InputStream fontis = is){
                        return Font.createFont(Font.TRUETYPE_FONT, fontis);
                    } catch (FontFormatException | IOException e1) {
                        return null;
                    }
                }).filter(Objects::nonNull)
                .filter(ge::registerFont)
                .collect(Collectors.toSet());
    }
    
    public void loadFontSymbol(String fontName, String fontSymbol, InputStream is) {
        BufferedImage bi = Capture.load(is);
        loadFontSymbol(fontName, fontSymbol, bi);
    }
    
    public void loadFontSymbol(String fontName, String fontSymbol, BufferedImage bi) {
        FontFamily ff = getOrCreateFontFamily(fontName);
        FontSymbol f = new FontSymbol(ff, fontSymbol, bi);
        ff.add(f);
    }
    
    private FontFamily getOrCreateFontFamily(String fontName) {
        return fontFamily.computeIfAbsent(fontName, FontFamily::new);
    }

    public String recognize(BufferedImage bi) {
        ImageBinary i = new ImageBinaryGrey(bi);

        return recognize(i);
    }

    public String recognize(ImageBinary i) {
        List<FontSymbol> list = getSymbols();

        return recognize(i, 0, 0, i.getWidth() - 1, i.getHeight() - 1, list);
    }

    /**
     * 
     * @param fontSet
     *            use font in the specified folder only
     * @param bi
     * @return
     */
    public String recognize(BufferedImage bi, String fontSet) {
        ImageBinary i = new ImageBinaryGrey(bi);

        return recognize(i, fontSet);
    }

    public String recognize(ImageBinary i, String fontSet) {
        List<FontSymbol> list = getSymbols(fontSet);

        return recognize(i, 0, 0, i.getWidth() - 1, i.getHeight() - 1, list);
    }

    public String recognize(ImageBinary i, int x1, int y1, int x2, int y2) {
        List<FontSymbol> list = getSymbols();

        return recognize(i, x1, y1, x2, y2, list);
    }

    public String recognize(ImageBinary i, int x1, int y1, int x2, int y2, String fontFamily) {
        List<FontSymbol> list = getSymbols(fontFamily);

        return recognize(i, x1, y1, x2, y2, list);
    }

    public String recognize(ImageBinary i, int x1, int y1, int x2, int y2, List<FontSymbol> list) {
        Set<FontSymbolLookup> all = findAll(list, i, x1, y1, x2, y2);

        return recognize(all);
    }

    public String recognize(Set<FontSymbolLookup> all) {
        StringBuilder str = new StringBuilder();

        if (all.size() == 0)
            return "";
        

        // sort top/bottom/left/right
        List<FontSymbolLookup> recognizedSymbol = all.stream().sorted(new Left2Right()).collect(Collectors.toList());

        // calculate rows
    
        Iterator<FontSymbolLookup> symbolIter = recognizedSymbol.iterator();
        FontSymbolLookup prev = symbolIter.next();
        while (symbolIter.hasNext()) {
            FontSymbolLookup next = symbolIter.next();
            if(prev.cross(next) && prev.intersectionAreaRatio(next) > 0.1d) { // if both symbol are in conflict
                double sizeRatio = (double) prev.size() / (double) next.size();
                if(sizeRatio > 1.33d) { // prev is much bigger, so we take it
                    next = prev;
                } else if (sizeRatio > 0.75d){ // prev and next are similar in size
                    if(prev.g > next.g) { // prev is of better quality
                        next = prev;
                    }
                }
                
            } else {
                str.append(prev.fs.fontSymbol);
                if(!next.yCross(prev)) { // new line
                    str.append("\n");
                } else if(next.x - (prev.x + prev.fs.getWidth()) > Math.min(next.getWidth(), prev.getWidth())*0.7) { // space
                    str.append(" ");
                }
            }
            prev = next;
        }
        str.append(prev.fs.fontSymbol);
        return str.toString();
    }

}
