package com.github.axet.lookup;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.github.axet.lookup.common.FontFamily;
import com.github.axet.lookup.common.FontSymbol;
import com.github.axet.lookup.common.FontSymbolLookup;
import com.github.axet.lookup.common.GPoint;
import com.github.axet.lookup.common.ImageBinary;
import com.github.axet.lookup.common.ImageBinaryGrey;
import com.github.axet.lookup.common.LessCompare;
import com.github.axet.lookup.proc.CannyEdgeDetector;
import com.github.axet.lookup.proc.NCC;

public class OCRCore {
    
    /**
     * sort elements from top to bottom and left to right.
     * elements on the same line are sorted left to right, even if they are not exactly at the same height,
     * this can cause inconsistency in the sort but should not be a problem for properly aligned text.
     */
    static public class Left2Right implements Comparator<FontSymbolLookup> {
        @Override
        public int compare(FontSymbolLookup arg0, FontSymbolLookup arg1) {
           
            if(arg0.yCross(arg1)) { // if both symbol are on the same line
                return LessCompare.compareSmallFirst(arg0.x, arg1.x); // compare horizontally
            } else {
                return LessCompare.compareSmallFirst(arg0.y, arg1.y); // compare vertically
            }
        }
    }

    public Map<String, FontFamily> fontFamily = new HashMap<String, FontFamily>();

    public CannyEdgeDetector detector = new CannyEdgeDetector();

    // 1.0f == exact match, -1.0f - completely different images
    public float threshold = 0.70f;

    public OCRCore(float threshold) {
        this.threshold = threshold;

        detector.setLowThreshold(3f);
        detector.setHighThreshold(3f);
        detector.setGaussianKernelWidth(2);
        detector.setGaussianKernelRadius(1f);
    }

    public List<FontSymbol> getSymbols() {
        List<FontSymbol> list = new ArrayList<FontSymbol>();

        for (FontFamily f : fontFamily.values()) {
            list.addAll(f);
        }

        return list;
    }

    public List<FontSymbol> getSymbols(String fontFamily) {
        return this.fontFamily.get(fontFamily);
    }

    public Set<FontSymbolLookup> findAll(List<FontSymbol> list, ImageBinaryGrey bi) {
        return findAll(list, bi, 0, 0, bi.getWidth(), bi.getHeight());
    }

    public Set<FontSymbolLookup> findAll(List<FontSymbol> list, ImageBinary bi, int x1, int y1, int x2, int y2) {
        Set<FontSymbolLookup> l = new HashSet<FontSymbolLookup>();

        for (FontSymbol fs : list) {
            for (ImageBinary imageScaleBin : fs.image.scales) {
                List<GPoint> ll = NCC.lookupAll(bi, x1, y1, x2, y2, imageScaleBin, threshold);
                for (GPoint p : ll)
                    l.add(new FontSymbolLookup(fs, p.x, p.y, p.g));
            }
        }

        return l;
    }

}
