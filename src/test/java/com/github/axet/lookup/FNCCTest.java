package com.github.axet.lookup;

import com.github.axet.lookup.common.GPoint;
import com.github.axet.lookup.common.ImageBinaryGrey;
import com.github.axet.lookup.common.ImageBinaryGreyFeature;
import com.github.axet.lookup.common.ImageBinaryRGB;
import com.github.axet.lookup.common.ImageBinaryRGBFeature;
import com.github.axet.lookup.proc.FNCC;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.awt.image.BufferedImage;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;


@RunWith(BlockJUnit4ClassRunner.class)
public class FNCCTest {
    
    private BufferedImage image;
    private BufferedImage template;
    
    public FNCCTest() {
        image = Capture.load(OCRTest.class, "cyclopst1.png");
        template = Capture.load(OCRTest.class, "cyclopst3.png");
    }
    
    @Test
    public void testRGB() {
        List<GPoint> pp = FNCC.lookupAll(new ImageBinaryRGB(image), new ImageBinaryRGBFeature(template, 5000), 0.9f);
        
        assertThat(pp, hasSize(1));
        assertThat(pp.get(0), allOf(hasProperty("x", is(21d)), hasProperty("y", is(7d))));
       
    }
    
    @Test
    public void testGrayScale() {
        List<GPoint> pp = FNCC.lookupAll(new ImageBinaryGrey(image), new ImageBinaryGreyFeature(template, 5000),
                0.9f);
    
        assertThat(pp, hasSize(1));
        assertThat(pp.get(0), allOf(hasProperty("x", is(21d)), hasProperty("y", is(7d))));
    }
}
