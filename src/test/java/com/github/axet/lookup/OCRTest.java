package com.github.axet.lookup;

import java.io.File;

import com.github.axet.lookup.common.ImageBinaryGrey;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class OCRTest {
    
    private final OCR ocr;
    
    public OCRTest() {
        ocr = new OCR(0.70f);
        // will go to com/github/axet/lookup/fonts folder and load all font
        // families (here is only font_1 family in this library)
        ocr.loadFontsDirectory(OCRTest.class, new File("fonts"));
    
        // example how to load only one family
        // "com/github/axet/lookup/fonts/font_1"
        ocr.loadFont(OCRTest.class, new File("fonts", "font_1"));
    }
    
    @Test
    public void testWithAllFonts() {
        String result = ocr.recognize(Capture.load(OCRTest.class, "test3.png"));
        assertThat(result, Matchers.is("3662\n3 2€/€\u200B"));
    }
    
    @Test
    public void testWithOneFont() {
        String result = ocr.recognize(Capture.load(OCRTest.class, "test3.png"), "font_1");
        assertThat(result, Matchers.is("3662\n3 2€/€\u200B"));
    }
    
    @Test
    public void testWithRectangle() {
        ImageBinaryGrey i = new ImageBinaryGrey(Capture.load(OCRTest.class, "full.png"));
        String result = ocr.recognize(i, 1285, 654, 1343, 677, "font_1");
        assertThat(result, Matchers.is("4339"));
    }
}
