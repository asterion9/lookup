package com.github.axet.lookup;

import java.awt.image.BufferedImage;
import java.util.List;

import com.github.axet.lookup.common.GPoint;
import com.github.axet.lookup.common.ImageBinaryGrey;
import com.github.axet.lookup.common.ImageBinaryRGB;
import com.github.axet.lookup.proc.NCC;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@RunWith(BlockJUnit4ClassRunner.class)
public class NCCTest {
    
    private BufferedImage image;
    private BufferedImage template;
    
    public NCCTest() {
        image = Capture.load(OCRTest.class, "cyclopst1.png");
        template = Capture.load(OCRTest.class, "cyclopst3.png");
    }
    
    @Test
    public void testNccRgb() {
        List<GPoint> pp = NCC.lookupAll(new ImageBinaryRGB(image), new ImageBinaryRGB(template), 0.9f);
    
        assertThat(pp, hasSize(1));
        assertThat(pp.get(0), allOf(hasProperty("x", is(21d)), hasProperty("y", is(7d))));
    
    }
    
    @Test
    public void testNccGreyScale() {
        List<GPoint> pp = NCC.lookupAll(new ImageBinaryGrey(image), new ImageBinaryGrey(template), 0.9f);
    
        assertThat(pp, hasSize(1));
        assertThat(pp.get(0), allOf(hasProperty("x", is(21d)), hasProperty("y", is(7d))));
    
    }
}
