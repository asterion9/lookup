package com.github.axet.lookup;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.github.axet.lookup.common.GFirst;
import com.github.axet.lookup.common.GPoint;
import com.github.axet.lookup.common.GSPoint;
import com.github.axet.lookup.common.ImageBinaryGreyScaleRGB;
import com.github.axet.lookup.common.ImageBinaryScale;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

@RunWith(BlockJUnit4ClassRunner.class)
public class SNCCTest {

    @Test
    public void testSncc() {
        BufferedImage image = Capture.load(OCRTest.class, "desktop.png");
        BufferedImage template = Capture.load(OCRTest.class, "desktop_feature_big.png");
        
        ImageBinaryScale si = new ImageBinaryGreyScaleRGB(image);
        ImageBinaryScale st = new ImageBinaryGreyScaleRGB(template);
    
        String result = IntStream.rangeClosed(2, 6)
                .mapToDouble(i -> 1d / i)
                .mapToObj(scale -> {
                    long startTime = System.currentTimeMillis();
                    List<GSPoint> pp = searchAtScale(si, st, scale);
                    long duration = System.currentTimeMillis() - startTime;
                    boolean testOk = pp.size() == 1 && pp.get(0).x == 584 && pp.get(0).y == 540;
                    //assertThat(pp, hasSize(1));
                    //assertThat(pp.get(0), allOf(hasProperty("x", is(584d)), hasProperty("y", is(540d))));
                    assertTrue(testOk);
                    return "scale " + scale + " : result=" + testOk + ", duration="+duration;
                }).collect(Collectors.joining("\n"));
    
        System.out.println(result);
        //test reult are interesting, up to a certain scale, the performance decrease
    /*
        Long l;

        for (int i = 0; i < 2; i++) {
            l = System.currentTimeMillis();
            {
                List<GSPoint> pp = s.lookupAll(si, st);

                Collections.sort(pp, new GFirst());

                for (GPoint p : pp) {
                    System.out.println(p);
                }
            }
            System.out.println(System.currentTimeMillis() - l);
        }*/
    }
    
    private List<GSPoint> searchAtScale(ImageBinaryScale si, ImageBinaryScale st, double scale) {
        LookupScale s = new LookupScale(scale, (int) (1/scale), 0.60f, 0.9f);
        List<GSPoint> pp = s.lookupAll(si, st);
        
        Collections.sort(pp, new GFirst());
        return pp;
    }
    
    
}
