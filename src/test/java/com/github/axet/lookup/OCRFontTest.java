package com.github.axet.lookup;

import com.github.axet.lookup.common.ImageBinaryGrey;
import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class OCRFontTest {
	private final OCR ocr;
	
	public OCRFontTest() {
		ocr = new OCR(0.80f);
		// will go to com/github/axet/lookup/fonts folder and load all font
		// families (here is only font_1 family in this library)
		ocr.loadFontTtf(OCRFontTest.class, new File("roboto"), 34);
	}
	
	@Test
	@Ignore
	//not working
	public void testHeader() {
		ImageBinaryGrey i = new ImageBinaryGrey(Capture.load(OCRFontTest.class, "test-roboto.png"));
		String result = ocr.recognize(i);
		assertThat(result, Matchers.is("Historique"));
	}
}
