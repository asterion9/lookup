package com.github.axet.lookup;

import com.github.axet.lookup.common.SArray;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class SArrayTest {

    @Test
    public void testSarray() {
        int[] s = new int[] {

        5, 2, 5, 2,

        3, 6, 3, 6,

        5, 2, 5, 2,

        3, 6, 3, 6

        };

        SArray a = new SArray();
        a.cx = 4;
        a.cy = 4;
        a.s = new double[a.cx * a.cy];

        for (int y = 0; y < a.cy; y++) {
            for (int x = 0; x < a.cx; x++) {
                a.s(x, y, a.s(x, y) + a.s(x - 1, y) + a.s(x, y - 1) - a.s(x - 1, y - 1));
            }
        }
    }
}
