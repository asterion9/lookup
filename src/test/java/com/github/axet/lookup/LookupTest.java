package com.github.axet.lookup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@RunWith(BlockJUnit4ClassRunner.class)
public class LookupTest {

    @Test
    public  void testLookup() {
        BufferedImage image = Capture.load(OCRTest.class, "cyclopst1.png");
        BufferedImage template = Capture.load(OCRTest.class, "cyclopst3.png");

        List<Point> pp = LookupColor.lookupAll(image, template, 0.2f);
    
        assertThat(pp, hasSize(1));
        assertThat(pp.get(0), allOf(hasProperty("x", is(37d)), hasProperty("y", is(19d))));
    }
}
